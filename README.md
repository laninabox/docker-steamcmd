## Tags

* `latest` - based on ubuntu:latest
* `centos` - based on centos:latest
* `debian` - based on debian:latest

## Usage

The purpose of this image is to run the latest steamCMD binary on your OS of choice.

You can for example get the CS:GO gamefiles and afterwards run you CS:GO Dedicated Server in a docker container.
All by using this single image, you have to do it in two steps though.


### Example: Counter-Strike: Global Offensive Dedicated Server

First update your local gamefiles (example directory is `/home/csgo` on your Docker host system):

    docker run --rm -ti \
               -v /home/csgo:/home/steam/csgo \
               laninabox/steamcmd \
               steamcmd_linux/steamcmd.sh \
               +login anonymous \
               +force_install_dir /home/steam/csgo/ \
               +app_update 740 validate \
               +quit

Then you can run a gameserver as follows:

    docker run --rm -ti --name csgoserver \
               -v /home/csgo:/home/steam/csgo \
               -p 27015:27015 -p 27015:27015/udp \
               laninabox/steamcmd \
               csgo/srcds_run \

This will start a csgo server on the LAN network with it's default configuration on the IP of the Docker host.
More startup options can be found here: https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Dedicated_Servers#Starting_the_Server

